package com.contactos.contactos.service;

import java.util.List;
import java.util.Optional;

import com.contactos.contactos.domain.Contacto;

public interface IContactosService {

	List<Contacto> buscarTodos();
	Contacto findById(Integer id);
	void guardar(Contacto contacto);
	void delete(Integer id);
	
}
