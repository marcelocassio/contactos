package com.contactos.contactos.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.contactos.contactos.domain.Contacto;
import com.contactos.contactos.repository.IContactosRepository;
import com.contactos.contactos.service.IContactosService;

@Service
public class ContactoService implements IContactosService{

	@Autowired
	private IContactosRepository repoContactos;
	
	
	@Override
	public List<Contacto> buscarTodos() {
		
		return repoContactos.findAll();
	}


	@Override
	public void guardar(Contacto contacto) {
		repoContactos.save(contacto);
	}


	@Override
	public void delete(Integer id) {
		repoContactos.deleteById(id);
	}


	@Override
	public Contacto findById(Integer id) {
		return repoContactos.findById(id).orElse(null);
	}

}
