package com.contactos.contactos.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.contactos.contactos.domain.Contacto;
import com.contactos.contactos.service.IContactosService;

@RestController
@RequestMapping("/api")
public class ContactoController {
	
	@Autowired
	private IContactosService service;

	@GetMapping("/contactos")
	public List<Contacto> buscarTodo(){
		return service.buscarTodos();
	}
	
	@GetMapping("/contacto/{id}")
	public ResponseEntity<?> buscarPorId(@PathVariable("id") Integer id) {
		Contacto contacto = null;
		Map<String, Object> response = new HashMap<>();
		try {
			contacto = service.findById(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos: ");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().toString()));
			return new ResponseEntity<Map>(response, HttpStatus.NOT_FOUND);
		}
		if(contacto == null) {
			response.put("mensaje", "El contacto con ID: ".concat(id.toString().concat(" no existe en la base de datos")));
			return new ResponseEntity<Map>(response, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Contacto>(contacto, HttpStatus.OK);
	}
	
	@PostMapping("/save")
	public ResponseEntity<?> guardar(@RequestBody Contacto contacto) {
		Map<String, Object> response = new HashMap<>();
		try {
			service.guardar(contacto);
			response.put("message", "El contacto ".concat(contacto.getNombre().concat(" ").concat(contacto.getApellido()).concat(" fue guardado!")));
		} catch (DataAccessException e) {
			response.put("message", "Fallo en el guardado del contacto");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().toString()));
			return new ResponseEntity<Map>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Map>(response, HttpStatus.CREATED);
		
	}
	
	@PutMapping("/update")
	public ResponseEntity<?> update(@RequestBody Contacto contacto) {
		
		Map<String, Object> response = new HashMap<>();
		if(contacto == null) {
			response.put("mensaje", "El contacto con ID: ".concat(contacto.getId().toString().concat(" no existe en la base de datos")));
			return new ResponseEntity<Map>(response, HttpStatus.NOT_FOUND);
		}
		
		try {
			service.guardar(contacto);
			response.put("message", "El contacto ".concat(contacto.getNombre().concat(" ").concat(contacto.getApellido()).concat(" fue actualizado!")));
		} catch (DataAccessException e) {
			response.put("message", "Fallo al actualizar el contacto");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().toString()));
			return new ResponseEntity<Map>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Map>(response, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") Integer id) {
		Map<String, Object> response = new HashMap<>();
		try {
			service.delete(id);
			response.put("message", "El contacto con ID: ".concat(id.toString()).concat(" fue eliminado con exito!"));
		} catch (DataAccessException e) {
			response.put("message", "Fallo al eliminar el contacto");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().toString()));
			return new ResponseEntity<Map>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Map>(response, HttpStatus.OK);
		
	}
	
}
