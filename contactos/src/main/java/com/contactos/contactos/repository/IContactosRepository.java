package com.contactos.contactos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.contactos.contactos.domain.Contacto;

public interface IContactosRepository extends JpaRepository<Contacto, Integer> {

}
